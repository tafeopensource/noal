<?php
use Migrations\AbstractMigration;

class CreateWeekdays extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('weekdays');
        $table->addColumn('shortname', 'string', [
            'default' => null,
            'limit' => 3,
            'null' => false,
        ]);
        $table->addColumn('longname', 'string', [
            'default' => null,
            'limit' => 10,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addIndex([
            'shortname',
        ], [
            'name' => 'UNIQUE_SHORTNAME',
            'unique' => true,
        ]);
        $table->addIndex([
            'longname',
        ], [
            'name' => 'UNIQUE_LONGNAME',
            'unique' => true,
        ]);
        $table->create();
    }
}
