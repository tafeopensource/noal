<?php
use Phinx\Seed\AbstractSeed;

/**
 * Lecturers seed.
 */
class LecturersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'givenname' => 'Adrian',
                'lastname'  => 'Gould',
                'email'     => 'adrian.gould@smtafe.wa.edu.au',

            ],
            [
                'givenname' => 'Matthew',
                'lastname'  => 'Meyer',
                'email'     => 'matthew.meyer@smtafe.wa.edu.au',

            ],
            [
                'givenname' => 'Joshua',
                'lastname'  => 'Shoebridge',
                'email'     => 'joshua.shoebridge@smtafe.wa.edu.au',

            ],
            [
                'givenname' => 'Denis',
                'lastname'  => 'Coldham',
                'email'     => 'denis.coldham@smtafe.wa.edu.au',

            ],
            [
                'givenname' => 'Huia',
                'lastname'  => 'Hickey',
                'email'     => 'huia.hickey@smtafe.wa.edu.au',

            ],
            [
                'givenname' => 'April',
                'lastname'  => 'Owen',
                'email'     => 'april.owen@smtafe.wa.edu.au',

            ],
        ];

        $table = $this->table('lecturers');
        $table->insert($data)->save();
    }
}
