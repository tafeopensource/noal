<?php
use Phinx\Seed\AbstractSeed;

/**
 * Classes seed.
 */
class LecturerClassesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'lecturer_id'    => 1, // Adrian
                'room_id'        => 1, // C03
                'sessiontime_id' => 2, // 0900-1200
                'weekday_id'     => 1, // Mon
                'created'        => date('Y-m-d H:i:s'),
                'modified'       => date('Y-m-d H:i:s'),
            ],
            [
                'lecturer_id'    => 1, // Adrian
                'room_id'        => 9, // C52
                'sessiontime_id' => 6, // 1300-1500
                'weekday_id'     => 1, // Mon
                'created'        => date('Y-m-d H:i:s'),
                'modified'       => date('Y-m-d H:i:s'),
            ],

            [
                'lecturer_id'    => 1, // Adrian
                'room_id'        => 6, // 8G32
                'sessiontime_id' => 3, // 0900-1100
                'weekday_id'     => 3, // Wed
                'created'        => date('Y-m-d H:i:s'),
                'modified'       => date('Y-m-d H:i:s'),
            ],

            [
                'lecturer_id'    => 1, // Adrian
                'room_id'        => 6, // 8G32
                'sessiontime_id' => 7, // 1100-1300
                'weekday_id'     => 3, // Wed
                'created'        => date('Y-m-d H:i:s'),
                'modified'       => date('Y-m-d H:i:s'),
            ],
            [
                'lecturer_id'    => 1, // Adrian
                'room_id'        => 6, // 8G32
                'sessiontime_id' => 11, // 1400-1600
                'weekday_id'     => 3, // Wed
                'created'        => date('Y-m-d H:i:s'),
                'modified'       => date('Y-m-d H:i:s'),
            ],

            [
                'lecturer_id'    => 1, // Adrian
                'room_id'        => 6, // 8G32
                'sessiontime_id' => 2, // 0900-1200
                'weekday_id'     => 5, // Fri
                'created'        => date('Y-m-d H:i:s'),
                'modified'       => date('Y-m-d H:i:s'),
            ],
            [
                'lecturer_id'    => 1, // Adrian
                'room_id'        => 6, // 8G32
                'sessiontime_id' => 9, // 1300-1600
                'weekday_id'     => 5, // Fri
                'created'        => date('Y-m-d H:i:s'),
                'modified'       => date('Y-m-d H:i:s'),
            ],


        ];

        $table = $this->table('lecturer_classes');
        $table->insert($data)->save();
    }
}
