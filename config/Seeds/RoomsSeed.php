<?php
use Phinx\Seed\AbstractSeed;

/**
 * Rooms seed.
 */
class RoomsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'room'     => 'C03',
                'block'    => 'C Block',
                'campus'   => 'Midland',
                'roomtype' => 'Networking Lab'
            ],
            [
                'room'     => 'C23',
                'block'    => 'C Block',
                'campus'   => 'Midland',
                'roomtype' => 'Networking Lab'
            ],
            [
                'room'     => 'C67',
                'block'    => 'C Block',
                'campus'   => 'Midland',
                'roomtype' => 'Games Development Computing Lab'
            ],
            [
                'room'     => 'C49',
                'block'    => 'C Block',
                'campus'   => 'Midland',
                'roomtype' => 'Programming, Web and General Computing Lab'
            ],
            [
                'room'     => 'C51',
                'block'    => 'C Block',
                'campus'   => 'Midland',
                'roomtype' => 'Mac Lab'
            ],
            [
                'room'     => '8G32',
                'block'    => 'Block 8',
                'campus'   => 'Thornlie',
                'roomtype' => 'Mac Lab'
            ],
            [
                'room'     => '8G19',
                'block'    => 'Block 8',
                'campus'   => 'Thornlie',
                'roomtype' => 'Higher Education Common Room'
            ],
            [
                'room'     => '8G28',
                'block'    => 'Block 8',
                'campus'   => 'Thornlie',
                'roomtype' => 'Office [Principle Lecturer]'
            ],
            [
                'room'     => 'C52',
                'block'    => 'C Block',
                'campus'   => 'Midland',
                'roomtype' => 'General Computing Lab'
            ],
        ];

        $table = $this->table('rooms');
        $table->insert($data)->save();
    }
}
