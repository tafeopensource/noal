<?php
use Phinx\Seed\AbstractSeed;

/**
 * SessionTimes seed.
 */
class SessionTimesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {

        $data = [
            ['id' => 1, 'start' => '08:30', 'end' => '12:00', 'created' => date('Y-m-d H:i:s'), 'modified' => date('Y-m-d H:i:s'),],
            ['id' => 2, 'start' => '09:00', 'end' => '11:00', 'created' => date('Y-m-d H:i:s'), 'modified' => date('Y-m-d H:i:s'),],
            ['id' => 3, 'start' => '09:00', 'end' => '12:00', 'created' => date('Y-m-d H:i:s'), 'modified' => date('Y-m-d H:i:s'),],
            ['id' => 4, 'start' => '09:00', 'end' => '13:00', 'created' => date('Y-m-d H:i:s'), 'modified' => date('Y-m-d H:i:s'),],
            ['id' => 5, 'start' => '10:00', 'end' => '12:00', 'created' => date('Y-m-d H:i:s'), 'modified' => date('Y-m-d H:i:s'),],
            ['id' => 6, 'start' => '10:00', 'end' => '13:00', 'created' => date('Y-m-d H:i:s'), 'modified' => date('Y-m-d H:i:s'),],
            ['id' => 7, 'start' => '11:00', 'end' => '13:00', 'created' => date('Y-m-d H:i:s'), 'modified' => date('Y-m-d H:i:s'),],
            ['id' => 8, 'start' => '13:00', 'end' => '15:00', 'created' => date('Y-m-d H:i:s'), 'modified' => date('Y-m-d H:i:s'),],
            ['id' => 9, 'start' => '13:00', 'end' => '16:00', 'created' => date('Y-m-d H:i:s'), 'modified' => date('Y-m-d H:i:s'),],
            ['id' => 10, 'start' => '13:00', 'end' => '17:00', 'created' => date('Y-m-d H:i:s'), 'modified' => date('Y-m-d H:i:s'),],
            ['id' => 11, 'start' => '14:00', 'end' => '16:00', 'created' => date('Y-m-d H:i:s'), 'modified' => date('Y-m-d H:i:s'),],
            ['id' => 12, 'start' => '14:00', 'end' => '17:00', 'created' => date('Y-m-d H:i:s'), 'modified' => date('Y-m-d H:i:s'),],
            ['id' => 13, 'start' => '18:00', 'end' => '20:00', 'created' => date('Y-m-d H:i:s'), 'modified' => date('Y-m-d H:i:s'),],
            ['id' => 14, 'start' => '18:00', 'end' => '21:00', 'created' => date('Y-m-d H:i:s'), 'modified' => date('Y-m-d H:i:s'),],

        ];

        $table = $this->table('session_times');
        $table->insert($data)->save();
    }
}
