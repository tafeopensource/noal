<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $sessionTime->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $sessionTime->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Session Times'), ['action' => 'index']) ?></li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $sessionTime->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $sessionTime->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Session Times'), ['action' => 'index']) ?></li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($sessionTime); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Session Time']) ?></legend>
    <?php
    echo $this->Form->input('start');
    echo $this->Form->input('end');
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
