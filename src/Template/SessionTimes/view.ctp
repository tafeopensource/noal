<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Session Time'), ['action' => 'edit', $sessionTime->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Session Time'), ['action' => 'delete', $sessionTime->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sessionTime->id)]) ?> </li>
<li><?= $this->Html->link(__('List Session Times'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Session Time'), ['action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
<li><?= $this->Html->link(__('Edit Session Time'), ['action' => 'edit', $sessionTime->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Session Time'), ['action' => 'delete', $sessionTime->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sessionTime->id)]) ?> </li>
<li><?= $this->Html->link(__('List Session Times'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Session Time'), ['action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($sessionTime->id) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($sessionTime->id) ?></td>
        </tr>
        <tr>
            <td><?= __('Start') ?></td>
            <td><?= h($sessionTime->start) ?></td>
        </tr>
        <tr>
            <td><?= __('End') ?></td>
            <td><?= h($sessionTime->end) ?></td>
        </tr>
        <tr>
            <td><?= __('Created') ?></td>
            <td><?= h($sessionTime->created) ?></td>
        </tr>
        <tr>
            <td><?= __('Modified') ?></td>
            <td><?= h($sessionTime->modified) ?></td>
        </tr>
    </table>
</div>

