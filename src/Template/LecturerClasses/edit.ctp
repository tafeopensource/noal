<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $lecturerClass->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $lecturerClass->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Lecturer Classes'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Lecturers'), ['controller' => 'Lecturers', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Lecturer'), ['controller' => 'Lecturers', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Rooms'), ['controller' => 'Rooms', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Room'), ['controller' => 'Rooms', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Sessiontimes'), ['controller' => 'SessionTimes', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Sessiontime'), ['controller' => 'SessionTimes', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Weekdays'), ['controller' => 'Weekdays', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Weekday'), ['controller' => 'Weekdays', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $lecturerClass->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $lecturerClass->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Lecturer Classes'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Lecturers'), ['controller' => 'Lecturers', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Lecturer'), ['controller' => 'Lecturers', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Rooms'), ['controller' => 'Rooms', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Room'), ['controller' => 'Rooms', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Sessiontimes'), ['controller' => 'SessionTimes', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Sessiontime'), ['controller' => 'SessionTimes', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Weekdays'), ['controller' => 'Weekdays', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Weekday'), ['controller' => 'Weekdays', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($lecturerClass); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Lecturer Class']) ?></legend>
    <?php
    echo $this->Form->input('lecturer_id', ['options' => $lecturers]);
    echo $this->Form->input('room_id', ['options' => $rooms]);
    echo $this->Form->input('sessiontime_id', ['options' => $sessiontimes]);
    echo $this->Form->input('weekday_id', ['options' => $weekdays]);
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
