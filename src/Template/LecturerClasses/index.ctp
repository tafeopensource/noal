<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('title');
?>Classes<?php
$this->end();

$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('List Lecturer Classes'), ['controller' => 'LecturerClasses', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('List Lecturers'), ['controller' => 'Lecturers', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('List Rooms'), ['controller' => 'Rooms', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('List Session Times'), ['controller' => 'SessionTimes', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('List Weekdays'), ['controller' => 'Weekdays', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('New Lecturer Class'), ['action' => 'add']); ?></li>
<li><?= $this->Html->link(__('New Lecturer'), ['controller' => ' Lecturers', 'action' => 'add']); ?></li>
<li><?= $this->Html->link(__('New Room'), ['controller' => ' Rooms', 'action' => 'add']); ?></li>
<li><?= $this->Html->link(__('New Session Time'), ['controller' => ' SessionTimes', 'action' => 'add']); ?></li>
<li><?= $this->Html->link(__('New Weekday'), ['controller' => ' Weekdays', 'action' => 'add']); ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?>
<p>Current time: <?php echo \App\Model\Entity\SessionTime::time(time(),'Australia/Perth'); ?> (at last refresh)</p>
<table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
    <tr>
        <th><?= $this->Paginator->sort('id'); ?></th>
        <th><?= $this->Paginator->sort('lecturer_id'); ?></th>
        <th><?= $this->Paginator->sort('room_id'); ?></th>
        <th><?= $this->Paginator->sort('weekday_id', "Day"); ?></th>
        <th><?= $this->Paginator->sort('start', 'Session'); ?></th>
        <th class="actions"><?= __('Actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($lecturerClasses as $lecturerClass): ?>
        <tr>
            <td><?= $this->Number->format($lecturerClass->id) ?></td>
            <td>
                <?= $lecturerClass->has('lecturer') ? $this->Html->link($lecturerClass->lecturer->getFullname(),
                    ['controller' => 'Lecturers', 'action' => 'view', $lecturerClass->lecturer->id]) : '' ?>
            </td>
            <td>
                <?= $lecturerClass->has('room') ? $this->Html->link($lecturerClass->room->room, ['controller' => 'Rooms', 'action' => 'view', $lecturerClass->room->id]) : '' ?>
            </td>
            <td>
                <?= $lecturerClass->has('weekday') ? $this->Html->link($lecturerClass->weekday->shortname, ['controller' => 'Weekdays', 'action' => 'view', $lecturerClass->weekday->id]) :
                    '' ?>
            </td>
            <td>
                <?= $lecturerClass->has('sessiontime') ? $this->Html->link(\App\Model\Entity\SessionTime::time($lecturerClass->sessiontime->start) . ' - ' . \App\Model\Entity\SessionTime::time($lecturerClass->sessiontime->end),
                    ['controller' => 'SessionTimes', 'action' => 'view', $lecturerClass->sessiontime->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link('', ['action' => 'view', $lecturerClass->id], ['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                <?= $this->Html->link('', ['action' => 'edit', $lecturerClass->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                <?= $this->Form->postLink('', ['action' => 'delete', $lecturerClass->id],
                    ['confirm' => __('Are you sure you want to delete # {0}?', $lecturerClass->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
