<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Lecturer Class'), ['action' => 'edit', $lecturerClass->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Lecturer Class'), ['action' => 'delete', $lecturerClass->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lecturerClass->id)]) ?> </li>
<li><?= $this->Html->link(__('List Lecturer Classes'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Lecturer Class'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Lecturers'), ['controller' => 'Lecturers', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Lecturer'), ['controller' => 'Lecturers', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Rooms'), ['controller' => 'Rooms', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Room'), ['controller' => 'Rooms', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Sessiontimes'), ['controller' => 'SessionTimes', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Sessiontime'), ['controller' => 'SessionTimes', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Weekdays'), ['controller' => 'Weekdays', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Weekday'), ['controller' => 'Weekdays', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
<li><?= $this->Html->link(__('Edit Lecturer Class'), ['action' => 'edit', $lecturerClass->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Lecturer Class'), ['action' => 'delete', $lecturerClass->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lecturerClass->id)]) ?> </li>
<li><?= $this->Html->link(__('List Lecturer Classes'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Lecturer Class'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Lecturers'), ['controller' => 'Lecturers', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Lecturer'), ['controller' => 'Lecturers', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Rooms'), ['controller' => 'Rooms', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Room'), ['controller' => 'Rooms', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Sessiontimes'), ['controller' => 'SessionTimes', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Sessiontime'), ['controller' => 'SessionTimes', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Weekdays'), ['controller' => 'Weekdays', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Weekday'), ['controller' => 'Weekdays', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($lecturerClass->id) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Lecturer') ?></td>
            <td><?= $lecturerClass->has('lecturer') ? $this->Html->link($lecturerClass->lecturer->id, ['controller' => 'Lecturers', 'action' => 'view', $lecturerClass->lecturer->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('Room') ?></td>
            <td><?= $lecturerClass->has('room') ? $this->Html->link($lecturerClass->room->id, ['controller' => 'Rooms', 'action' => 'view', $lecturerClass->room->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('Sessiontime') ?></td>
            <td><?= $lecturerClass->has('sessiontime') ? $this->Html->link($lecturerClass->sessiontime->id, ['controller' => 'SessionTimes', 'action' => 'view', $lecturerClass->sessiontime->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('Weekday') ?></td>
            <td><?= $lecturerClass->has('weekday') ? $this->Html->link($lecturerClass->weekday->id, ['controller' => 'Weekdays', 'action' => 'view', $lecturerClass->weekday->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($lecturerClass->id) ?></td>
        </tr>
        <tr>
            <td><?= __('Created') ?></td>
            <td><?= h($lecturerClass->created) ?></td>
        </tr>
        <tr>
            <td><?= __('Modified') ?></td>
            <td><?= h($lecturerClass->modified) ?></td>
        </tr>
    </table>
</div>

