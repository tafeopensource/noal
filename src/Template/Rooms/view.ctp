<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Room'), ['action' => 'edit', $room->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Room'), ['action' => 'delete', $room->id], ['confirm' => __('Are you sure you want to delete # {0}?', $room->id)]) ?> </li>
<li><?= $this->Html->link(__('List Rooms'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Room'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Lecturer Classes'), ['controller' => 'LecturerClasses', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Lecturer Class'), ['controller' => 'LecturerClasses', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
<li><?= $this->Html->link(__('Edit Room'), ['action' => 'edit', $room->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Room'), ['action' => 'delete', $room->id], ['confirm' => __('Are you sure you want to delete # {0}?', $room->id)]) ?> </li>
<li><?= $this->Html->link(__('List Rooms'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Room'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Lecturer Classes'), ['controller' => 'LecturerClasses', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Lecturer Class'), ['controller' => 'LecturerClasses', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($room->id) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Room') ?></td>
            <td><?= h($room->room) ?></td>
        </tr>
        <tr>
            <td><?= __('Block') ?></td>
            <td><?= h($room->block) ?></td>
        </tr>
        <tr>
            <td><?= __('Campus') ?></td>
            <td><?= h($room->campus) ?></td>
        </tr>
        <tr>
            <td><?= __('Roomtype') ?></td>
            <td><?= h($room->roomtype) ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($room->id) ?></td>
        </tr>
        <tr>
            <td><?= __('Created') ?></td>
            <td><?= h($room->created) ?></td>
        </tr>
        <tr>
            <td><?= __('Modified') ?></td>
            <td><?= h($room->modified) ?></td>
        </tr>
    </table>
</div>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Related LecturerClasses') ?></h3>
    </div>
    <?php if (!empty($room->lecturer_classes)): ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Lecturer Id') ?></th>
                <th><?= __('Room Id') ?></th>
                <th><?= __('Sessiontime Id') ?></th>
                <th><?= __('Weekday Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($room->lecturer_classes as $lecturerClasses): ?>
                <tr>
                    <td><?= h($lecturerClasses->id) ?></td>
                    <td><?= h($lecturerClasses->lecturer_id) ?></td>
                    <td><?= h($lecturerClasses->room_id) ?></td>
                    <td><?= h($lecturerClasses->sessiontime_id) ?></td>
                    <td><?= h($lecturerClasses->weekday_id) ?></td>
                    <td><?= h($lecturerClasses->created) ?></td>
                    <td><?= h($lecturerClasses->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link('', ['controller' => 'LecturerClasses', 'action' => 'view', $lecturerClasses->id], ['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                        <?= $this->Html->link('', ['controller' => 'LecturerClasses', 'action' => 'edit', $lecturerClasses->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                        <?= $this->Form->postLink('', ['controller' => 'LecturerClasses', 'action' => 'delete', $lecturerClasses->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lecturerClasses->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p class="panel-body">no related LecturerClasses</p>
    <?php endif; ?>
</div>
