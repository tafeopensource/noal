<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('List Lecturers'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Lecturer Classes'), ['controller' => 'LecturerClasses', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Lecturer Class'), ['controller' => 'LecturerClasses', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?= $this->Html->link(__('List Lecturers'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Lecturer Classes'), ['controller' => 'LecturerClasses', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Lecturer Class'), ['controller' => 'LecturerClasses', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($lecturer); ?>
<fieldset>
    <legend><?= __('Add {0}', ['Lecturer']) ?></legend>
    <?php
    echo $this->Form->input('givenname');
    echo $this->Form->input('lastname');
    echo $this->Form->input('email');
    ?>
</fieldset>
<?= $this->Form->button(__("Add")); ?>
<?= $this->Form->end() ?>
