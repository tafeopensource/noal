<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('List Lecturer Classes'), ['controller' => 'LecturerClasses', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('List Lecturers'), ['controller' => 'Lecturers', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('List Rooms'), ['controller' => 'Rooms', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('List Session Times'), ['controller' => 'SessionTimes', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('List Weekdays'), ['controller' => 'Weekdays', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('New Lecturer Class'), ['action' => 'add']); ?></li>
<li><?= $this->Html->link(__('New Lecturer'), ['controller' => ' Lecturers', 'action' => 'add']); ?></li>
<li><?= $this->Html->link(__('New Room'), ['controller' => ' Rooms', 'action' => 'add']); ?></li>
<li><?= $this->Html->link(__('New Session Time'), ['controller' => ' SessionTimes', 'action' => 'add']); ?></li>
<li><?= $this->Html->link(__('New Weekday'), ['controller' => ' Weekdays', 'action' => 'add']); ?></li>
<?php $this->end(); ?>

<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?>

<table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id'); ?></th>
            <th><?= $this->Paginator->sort('givenname'); ?></th>
            <th><?= $this->Paginator->sort('lastname'); ?></th>
            <th><?= $this->Paginator->sort('email'); ?></th>
            <th class="actions"><?= __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($lecturers as $lecturer): ?>
        <tr>
            <td><?= $this->Number->format($lecturer->id) ?></td>
            <td><?= h($lecturer->givenname) ?></td>
            <td><?= h($lecturer->lastname) ?></td>
            <td><?= h($lecturer->email) ?></td>
            <td class="actions">
                <?= $this->Html->link('', ['action' => 'view', $lecturer->id], ['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                <?= $this->Html->link('', ['action' => 'edit', $lecturer->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                <?= $this->Form->postLink('', ['action' => 'delete', $lecturer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lecturer->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
