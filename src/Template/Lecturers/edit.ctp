<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $lecturer->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $lecturer->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Lecturers'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Lecturer Classes'), ['controller' => 'LecturerClasses', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Lecturer Class'), ['controller' => 'LecturerClasses', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $lecturer->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $lecturer->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Lecturers'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Lecturer Classes'), ['controller' => 'LecturerClasses', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Lecturer Class'), ['controller' => 'LecturerClasses', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($lecturer); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Lecturer']) ?></legend>
    <?php
    echo $this->Form->input('givenname');
    echo $this->Form->input('lastname');
    echo $this->Form->input('email');
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
