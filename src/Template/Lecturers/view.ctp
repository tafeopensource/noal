<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Lecturer'), ['action' => 'edit', $lecturer->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Lecturer'), ['action' => 'delete', $lecturer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lecturer->id)]) ?> </li>
<li><?= $this->Html->link(__('List Lecturers'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Lecturer'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Lecturer Classes'), ['controller' => 'LecturerClasses', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Lecturer Class'), ['controller' => 'LecturerClasses', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
<li><?= $this->Html->link(__('Edit Lecturer'), ['action' => 'edit', $lecturer->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Lecturer'), ['action' => 'delete', $lecturer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lecturer->id)]) ?> </li>
<li><?= $this->Html->link(__('List Lecturers'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Lecturer'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Lecturer Classes'), ['controller' => 'LecturerClasses', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Lecturer Class'), ['controller' => 'LecturerClasses', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($lecturer->id) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Givenname') ?></td>
            <td><?= h($lecturer->givenname) ?></td>
        </tr>
        <tr>
            <td><?= __('Lastname') ?></td>
            <td><?= h($lecturer->lastname) ?></td>
        </tr>
        <tr>
            <td><?= __('Email') ?></td>
            <td><?= h($lecturer->email) ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($lecturer->id) ?></td>
        </tr>
        <tr>
            <td><?= __('Created') ?></td>
            <td><?= h($lecturer->created) ?></td>
        </tr>
        <tr>
            <td><?= __('Modified') ?></td>
            <td><?= h($lecturer->modified) ?></td>
        </tr>
    </table>
</div>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Related LecturerClasses') ?></h3>
    </div>
    <?php if (!empty($lecturer->lecturer_classes)): ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Lecturer Id') ?></th>
                <th><?= __('Room Id') ?></th>
                <th><?= __('Sessiontime Id') ?></th>
                <th><?= __('Weekday Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($lecturer->lecturer_classes as $lecturerClasses): ?>
                <tr>
                    <td><?= h($lecturerClasses->id) ?></td>
                    <td><?= h($lecturerClasses->lecturer_id) ?></td>
                    <td><?= h($lecturerClasses->room_id) ?></td>
                    <td><?= h($lecturerClasses->sessiontime_id) ?></td>
                    <td><?= h($lecturerClasses->weekday_id) ?></td>
                    <td><?= h($lecturerClasses->created) ?></td>
                    <td><?= h($lecturerClasses->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link('', ['controller' => 'LecturerClasses', 'action' => 'view', $lecturerClasses->id], ['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                        <?= $this->Html->link('', ['controller' => 'LecturerClasses', 'action' => 'edit', $lecturerClasses->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                        <?= $this->Form->postLink('', ['controller' => 'LecturerClasses', 'action' => 'delete', $lecturerClasses->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lecturerClasses->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p class="panel-body">no related LecturerClasses</p>
    <?php endif; ?>
</div>
