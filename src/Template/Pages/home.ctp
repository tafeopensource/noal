<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;


/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('title');
?>Dashboard<?php
$this->end();


if (!Configure::read('debug')):
    throw new NotFoundException('Please replace src/Template/Pages/home.ctp with your own version.');
endif;

$cakeDescription = 'NoAL: No Absent Lecturers';

$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('List Lecturer Classes'), ['controller' => 'LecturerClasses', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('List Lecturers'), ['controller' => 'Lecturers', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('List Rooms'), ['controller' => 'Rooms', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('List Session Times'), ['controller' => 'SessionTimes', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('List Weekdays'), ['controller' => 'Weekdays', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('New Lecturer Class'), ['action' => 'add']); ?></li>
<li><?= $this->Html->link(__('New Lecturer'), ['controller' => ' Lecturers', 'action' => 'add']); ?></li>
<li><?= $this->Html->link(__('New Room'), ['controller' => ' Rooms', 'action' => 'add']); ?></li>
<li><?= $this->Html->link(__('New Session Time'), ['controller' => ' SessionTimes', 'action' => 'add']); ?></li>
<li><?= $this->Html->link(__('New Weekday'), ['controller' => ' Weekdays', 'action' => 'add']); ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-lg-3">One</div>
    <div class="col-xs-12 col-sm-6 col-lg-3">Two</div>
    <div class="col-xs-12 col-sm-6 col-lg-3">Three</div>
    <div class="col-xs-12 col-sm-6 col-lg-3">Four</div>
</div>


