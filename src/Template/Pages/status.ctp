<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;


/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('title');
?>Status<?php
$this->end();


if ( ! Configure::read('debug')):
    throw new NotFoundException('Please replace src/Template/Pages/home.ctp with your own version.');
endif;

$cakeDescription = 'NoAL: No Absent Lecturers';

$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('List Lecturer Classes'), ['controller' => 'LecturerClasses', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('List Lecturers'), ['controller' => 'Lecturers', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('List Rooms'), ['controller' => 'Rooms', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('List Session Times'), ['controller' => 'SessionTimes', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('List Weekdays'), ['controller' => 'Weekdays', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('New Lecturer Class'), ['action' => 'add']); ?></li>
<li><?= $this->Html->link(__('New Lecturer'), ['controller' => ' Lecturers', 'action' => 'add']); ?></li>
<li><?= $this->Html->link(__('New Room'), ['controller' => ' Rooms', 'action' => 'add']); ?></li>
<li><?= $this->Html->link(__('New Session Time'), ['controller' => ' SessionTimes', 'action' => 'add']); ?></li>
<li><?= $this->Html->link(__('New Weekday'), ['controller' => ' Weekdays', 'action' => 'add']); ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-lg-3">
        <h2>In Class</h2>
        <table class="table table-striped" cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                <th><?= $this->Paginator->sort('id'); ?></th>
                <th><?= $this->Paginator->sort('start'); ?></th>
                <th><?= $this->Paginator->sort('end'); ?></th>
                <th class="actions"><?= __('Actions'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($sessionTimes as $sessionTime): ?>
                <tr>
                    <td><?= $this->Number->format($sessionTime->id) ?></td>
                    <td><?= h(\App\Model\Entity\SessionTime::time($sessionTime->start)) ?></td>
                    <td><?= h(\App\Model\Entity\SessionTime::time($sessionTime->end)) ?></td>
                    <td class="actions">
                        <?= $this->Html->link('', ['action' => 'view', $sessionTime->id], ['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                        <?= $this->Html->link('', ['action' => 'edit', $sessionTime->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                        <?= $this->Form->postLink('', ['action' => 'delete', $sessionTime->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $sessionTime->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg-3">
        <h2>Absent</h2>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg-3">
        <h2>Available</h2>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg-3">
        <h2>On Leave</h2>
    </div>
</div>


