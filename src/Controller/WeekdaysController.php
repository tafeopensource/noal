<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Weekdays Controller
 *
 * @property \App\Model\Table\WeekdaysTable $Weekdays
 */
class WeekdaysController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'limit'=>5,
        ];
        $weekdays = $this->paginate($this->Weekdays);

        $this->set(compact('weekdays'));
        $this->set('_serialize', ['weekdays']);
    }

    /**
     * View method
     *
     * @param string|null $id Weekday id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $weekday = $this->Weekdays->get($id, [
            'contain' => ['LecturerClasses']
        ]);

        $this->set('weekday', $weekday);
        $this->set('_serialize', ['weekday']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $weekday = $this->Weekdays->newEntity();
        if ($this->request->is('post')) {
            $weekday = $this->Weekdays->patchEntity($weekday, $this->request->data);
            if ($this->Weekdays->save($weekday)) {
                $this->Flash->success(__('The weekday has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The weekday could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('weekday'));
        $this->set('_serialize', ['weekday']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Weekday id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $weekday = $this->Weekdays->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $weekday = $this->Weekdays->patchEntity($weekday, $this->request->data);
            if ($this->Weekdays->save($weekday)) {
                $this->Flash->success(__('The weekday has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The weekday could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('weekday'));
        $this->set('_serialize', ['weekday']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Weekday id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $weekday = $this->Weekdays->get($id);
        if ($this->Weekdays->delete($weekday)) {
            $this->Flash->success(__('The weekday has been deleted.'));
        } else {
            $this->Flash->error(__('The weekday could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
