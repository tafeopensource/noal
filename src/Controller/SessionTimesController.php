<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SessionTimes Controller
 *
 * @property \App\Model\Table\SessionTimesTable $SessionTimes
 */
class SessionTimesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'limit'=>5,
        ];
        $sessionTimes = $this->paginate($this->SessionTimes);

        $this->set(compact('sessionTimes'));
        $this->set('_serialize', ['sessionTimes']);
    }

    /**
     * View method
     *
     * @param string|null $id Session Time id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sessionTime = $this->SessionTimes->get($id, [
            'contain' => []
        ]);

        $this->set('sessionTime', $sessionTime);
        $this->set('_serialize', ['sessionTime']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sessionTime = $this->SessionTimes->newEntity();
        if ($this->request->is('post')) {
            $sessionTime = $this->SessionTimes->patchEntity($sessionTime, $this->request->data);
            if ($this->SessionTimes->save($sessionTime)) {
                $this->Flash->success(__('The session time has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The session time could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('sessionTime'));
        $this->set('_serialize', ['sessionTime']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Session Time id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sessionTime = $this->SessionTimes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sessionTime = $this->SessionTimes->patchEntity($sessionTime, $this->request->data);
            if ($this->SessionTimes->save($sessionTime)) {
                $this->Flash->success(__('The session time has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The session time could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('sessionTime'));
        $this->set('_serialize', ['sessionTime']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Session Time id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sessionTime = $this->SessionTimes->get($id);
        if ($this->SessionTimes->delete($sessionTime)) {
            $this->Flash->success(__('The session time has been deleted.'));
        } else {
            $this->Flash->error(__('The session time could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
