<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * LecturerClasses Controller
 *
 * @property \App\Model\Table\LecturerClassesTable $LecturerClasses
 */
class LecturerClassesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Lecturers', 'Rooms', 'Sessiontimes', 'Weekdays'],
            'limit'=>5,
        ];
        $lecturerClasses = $this->paginate($this->LecturerClasses);

        $this->set(compact('lecturerClasses'));
        $this->set('_serialize', ['lecturerClasses']);
    }

    /**
     * View method
     *
     * @param string|null $id Lecturer Class id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $lecturerClass = $this->LecturerClasses->get($id, [
            'contain' => ['Lecturers', 'Rooms', 'Sessiontimes', 'Weekdays']
        ]);

        $this->set('lecturerClass', $lecturerClass);
        $this->set('_serialize', ['lecturerClass']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $lecturerClass = $this->LecturerClasses->newEntity();
        if ($this->request->is('post')) {
            $lecturerClass = $this->LecturerClasses->patchEntity($lecturerClass, $this->request->data);
            if ($this->LecturerClasses->save($lecturerClass)) {
                $this->Flash->success(__('The lecturer class has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The lecturer class could not be saved. Please, try again.'));
            }
        }
        $lecturers = $this->LecturerClasses->Lecturers->find('list', ['limit' => 200]);
        $rooms = $this->LecturerClasses->Rooms->find('list', ['limit' => 200]);
        $sessiontimes = $this->LecturerClasses->Sessiontimes->find('list', ['limit' => 200]);
        $weekdays = $this->LecturerClasses->Weekdays->find('list', ['limit' => 200]);
        $this->set(compact('lecturerClass', 'lecturers', 'rooms', 'sessiontimes', 'weekdays'));
        $this->set('_serialize', ['lecturerClass']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Lecturer Class id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $lecturerClass = $this->LecturerClasses->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $lecturerClass = $this->LecturerClasses->patchEntity($lecturerClass, $this->request->data);
            if ($this->LecturerClasses->save($lecturerClass)) {
                $this->Flash->success(__('The lecturer class has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The lecturer class could not be saved. Please, try again.'));
            }
        }
        $lecturers = $this->LecturerClasses->Lecturers->find('list', ['limit' => 200]);
        $rooms = $this->LecturerClasses->Rooms->find('list', ['limit' => 200]);
        $sessiontimes = $this->LecturerClasses->Sessiontimes->find('list', ['limit' => 200]);
        $weekdays = $this->LecturerClasses->Weekdays->find('list', ['limit' => 200]);
        $this->set(compact('lecturerClass', 'lecturers', 'rooms', 'sessiontimes', 'weekdays'));
        $this->set('_serialize', ['lecturerClass']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Lecturer Class id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $lecturerClass = $this->LecturerClasses->get($id);
        if ($this->LecturerClasses->delete($lecturerClass)) {
            $this->Flash->success(__('The lecturer class has been deleted.'));
        } else {
            $this->Flash->error(__('The lecturer class could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
