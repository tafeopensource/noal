<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SessionTimesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SessionTimesTable Test Case
 */
class SessionTimesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SessionTimesTable
     */
    public $SessionTimes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.session_times'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SessionTimes') ? [] : ['className' => 'App\Model\Table\SessionTimesTable'];
        $this->SessionTimes = TableRegistry::get('SessionTimes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SessionTimes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
