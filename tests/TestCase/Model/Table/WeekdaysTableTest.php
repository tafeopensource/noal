<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WeekdaysTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WeekdaysTable Test Case
 */
class WeekdaysTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\WeekdaysTable
     */
    public $Weekdays;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.weekdays',
        'app.lecturer_classes',
        'app.lecturers',
        'app.rooms',
        'app.sessiontimes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Weekdays') ? [] : ['className' => 'App\Model\Table\WeekdaysTable'];
        $this->Weekdays = TableRegistry::get('Weekdays', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Weekdays);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
