<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LecturerClassesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LecturerClassesTable Test Case
 */
class LecturerClassesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LecturerClassesTable
     */
    public $LecturerClasses;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.lecturer_classes',
        'app.lecturers',
        'app.rooms',
        'app.sessiontimes',
        'app.weekdays'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('LecturerClasses') ? [] : ['className' => 'App\Model\Table\LecturerClassesTable'];
        $this->LecturerClasses = TableRegistry::get('LecturerClasses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LecturerClasses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
