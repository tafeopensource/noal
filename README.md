# The No Absentee Lecturers (NoAL) #

A centralised method of finding out who is able to cover a class when another member of staff is absent due to sickness, annual leave, long service leave, professional development, industry consultation or similar.

Open Source so as to allow other people to contribute, assist and provide advice.

![icon_256x256.png](https://bitbucket.org/repo/5kd88B/images/4241323139-icon_256x256.png)

## Wiki

The NoAL Wiki is found at [https://bitbucket.org/tafeopensource/noal/wiki/](https://bitbucket.org/tafeopensource/noal/wiki/)

The Feature List is found at [https://bitbucket.org/tafeopensource/noal/wiki/Possible%20Features](https://bitbucket.org/tafeopensource/noal/wiki/Possible%20Features)


## Software & Systems ##

#### Languages:    

- PHP 5.6+,     
- JavaScript,    
- HTML 5,     
- CSS 3,     
- Sass,     
- jQuery,     
- SQL

#### Database:      ####

- MariaDB (MySQL)

#### Frameworks: ####

- CakePHP 3.x,
- Bootstrap 3

### Note: ###

We are not going to deny that the inspiration came from some colleagues from a bit of fun. Since then, it has grown into a serous application concept.

### Disclaimer: ###

Names and likenesses of people and/or places, be they past, present, future, fictional or real that are used in this software are purely coincidental and are to be taken with a large pinch of salt and the humour they are intended to have.